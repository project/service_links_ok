Odnoklassniki - "Service Links" widget

author: Sandu Camerzan - https://www.drupal.org/u/sandu.camerzan
=========================================================================
"Odnoklassniki" Social Media share link, integrated with the module "Service Links".

This module was build with a dependency to the modules:

Service Links - https://www.drupal.org/project/service_links
Widget Services - included in the Service Links bundle.
=========================================================================
The module builds the share button for "Odnoklassniki", based on the API
described at the following link:
https://apiok.ru/wiki/pages/viewpage.action?pageId=42476656

The Odnoklassniki widget is built based on a request with the following parameters:

- sz: Size of the button. Can be 12, 20, 30, 45 - for text buttons,
      and 75, 100, 150 - for square buttons.

- st: Shape of the button. Can be: oval, rounded, straight.

- nc: No-counter. If selected (value=1) no counter is displayed.

- vt: Vertical align of button. If selected (value=1) the counter is displayed above.
  Otherwise, by default, the counter is displayed to the right.

- nt: No-text. If selected (value=1) no text is displayed inside the button.

- ck: Button text. Options are: 1 - "Класс", 2 - "Поделиться", 3 - "Нравиться".
      Only applicable for the "sz" param values of: 12, 20, 30, 45.
      If button has sizes > 45, no text is displayed inside the button.


