(function ($) {
  /**
   * Initiate the OK service widget button.
   */
  Drupal.behaviors.OK_service = {
    attach: function (context, settings) {
      // Get the params from passed settings or default to Drupal OK settings.
      var params = settings ? settings.ok_service_link : Drupal.settings.ok_service_link;
      console.log('OK settings: ' + JSON.stringify(params));

      // Retrieve the path setting and remove it from general list of settings.
      var path = (params.path == '<this>') ? document.URL : params.path;
      delete params.path;

      // Trigger the OK service button on all .service-odnoklassniki anchors.
      $('a.service-odnoklassniki', context).once('ok-init').each(function(){
        $(this).append('<div id="ok_shareWidget"></div>');

        !function (d, id, did, st) {
          var js = d.createElement("script");
          js.src = "https://connect.ok.ru/connect.js";
          js.onload = js.onreadystatechange = function () {
            if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
              if (!this.executed) {
                this.executed = true;
                setTimeout(function () {
                  OK.CONNECT.insertShareWidget(id,did,st);
                }, 0);
              }
            }};
          d.documentElement.appendChild(js);
        }(document,"ok_shareWidget", path, JSON.stringify(params));

      });
    }
  }
})(jQuery);